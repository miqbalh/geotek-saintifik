<!DOCTYPE html>
<html lang="en">
<head>
	@stack('head-prepend')
    @include('homepage.partial.header.head')
  @stack('head-append')
</head>

<body class="homepage">
	<header id="header">

		<!-- <div class="top-bar">
			<div class="container">
				<div class="row">
					@include('homepage.partial.header.social')
				</div>
			</div>
		</div> -->

		<nav class="navbar navbar-inverse" role="banner">
				<div class="container">
					@include('homepage.partial.header.nav')
				</div>
			</nav>
	</header>

	@yield('content')

	<footer id="footer" class="midnight-blue">
		<div class="container">
			<div class="row">
				@include('homepage.partial.footer.footer')
			</div>
		</div>
	</footer><!--/#footer-->

	@include('homepage.partial.footer.script')
</body>
</html>