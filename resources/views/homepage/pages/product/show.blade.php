@extends('homepage.layout.default')

@section('content')

<section id="blog" class="container">
  <div class="center">
    <h2>{{ $collection->title }}</h2>
    <!--<p class="lead">Pellentesque habitant morbi tristique senectus et netus et malesuada</p>-->
  </div>

  <div class="blog">
    <div class="row">
      <div class="col-md-12">
        <div class="blog-item">
          <img class="img-responsive img-blog" src="{{ asset(env('PATH_POST') .'/'.  $collection->image) }}" width="300px" alt="" />
          <div class="row">
            <div class="col-xs-12 col-sm-10 blog-content">
              <h2>{{ $collection->title }}</h2>
              <p>{!! $collection->description !!}</p>
            </div>
          </div>
        </div>
    </div>
  </div>
</section>

@endsection
