@extends('homepage.layout.default')

@section('content')

<section id="blog" class="container">
  <div class="center">
    <h2>{{ $category->name }}</h2>
    <!--<p class="lead">Pellentesque habitant morbi tristique senectus et netus et malesuada</p>-->
  </div>

  <div class="blog">
    <div class="row">
{{-- <ul> --}}
      @foreach ($collection as $item)
      {{-- <div class="row"> --}}
        {{-- <li> --}}
      <div class="col-md-4">
        <div class="blog-item">
            <div class="col-xs-12 col-sm-10 blog-content">
              <a href="{{ '/product/'. $category->slug .'/'. $item->slug }}">
                <img class="img-responsive img-blog" src="{{ asset(env('PATH_POST') .'/'.  $item->image) }}" width="100%" alt="" />
              </a>

              <h2 class="title-hidden">
                <a href="{{ '/product/'. $category->slug .'/'. $item->slug }}">
                  {{ $item->title }}
                </a>
              </h3>

              <h3 class="text-hidden">{!! $item->description !!}</h3>

              <a class="btn btn-primary readmore" href="{{ '/product/'. $category->slug .'/'. $item->slug }}">
                Read More
                <i class="fa fa-angle-right"></i>
              </a>
            </div>
          </div>
        </div>
      {{-- </li> --}}
        {{-- </div> --}}
      @endforeach
{{-- </ul> --}}
    </div>
  </div>
</section>

@endsection
