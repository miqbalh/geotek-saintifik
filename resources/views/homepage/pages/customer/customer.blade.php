@extends('homepage.layout.default')
@section('title', 'Customer')
@section('content')

<section id="portfolio">
  <div class="container">
    <div class="center">
      <h2>Our Customer</h2>

      <div class="mt5 w-50 center">
        <ul>
          @foreach ($collection as $item)
            <li class="tl">{{ $item->name }}</li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
</section>

@endsection
