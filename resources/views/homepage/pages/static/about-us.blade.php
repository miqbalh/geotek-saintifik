@extends('homepage.layout.default')
@section('title', 'About Us')
@section('content')

<section id="about-us">
  <div class="container">
    <div class="center wow fadeInDown">
      <h2>PT. GEOTEK SAINTIFIK INDONESIA</h2><br>
      <img src="template/images/logo/logo-text-large.png" height="200px">
      <p class="lead">PT. GEOTEK SANTIFIK INDONESIA was established in 2015 This company is engaged in services and Industrial Services, especially in the field of Laboratory Equipment. At present we are building a strong network in all Indonesian agencies including the Private Industry and Government sectors, Education and Research Centers and the Commercial sector. </p>
        <p class="lead">Our company is building a good networking with customers to get quality products that suit the needs of each customer.</p>
        <p class="lead">Always rely on the quality and innovate in terms of marketing products to ensure that these products are products that are in accordance with customer needs
          At present we are establishing good relationships with imported products, namely the best producers in the world to promote and sell high-tech products to facilitate and assist each customer in terms of research. and provide the best after-sales service to all our customers.</p>
        <!-- <br><br><b>Vision</b>
        <br><b>PT. GEOTEK SAINTIFIK INDONESIA</b> Having a vision to make this company the largest company in the field of laboratory equipment -->
    </div>
  </div><!--/.container-->
</section><!--/about-us-->

<section id="feature" >
  <div class="container">
    <div class="center wow fadeInDown">
      <h2 class="title mb3">
        Vision
      </h2>

      <div class="content mb6">
        <h3><b>PT. GEOTEK SAINTIFIK INDONESIA</b> Having a vision to make this company <br>the largest company in the field of laboratory equipment</h3>
      </div>

      <h2 class="title mb3">
        Mission
      </h2>

      <div class="content">
        <h3>"Doing work for customer satisfaction"</h3>
      </div>
    </div>
  </div><!--/.container-->
</section><!--/#feature-->

<section id="text-parners">
  <div class="container">
    <div class="center wow fadeInDown">
      <h2>Our Partners</h2>
      <!-- <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p> -->
    </div>

    <div class="partners">
      <ul>
        <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms" src="template/images/partners/QUALITEST-LOGO.jpg"></a></li>
        <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms" src="template/images/partners/TORONTECH -LOGO.jpg"></a></li>
        <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms" src="template/images/partners/Fuyi.png" width="50%"></a></li>
      </ul>
    </div>
  </div><!--/.container-->
</section><!--/#partner-->

@endsection
