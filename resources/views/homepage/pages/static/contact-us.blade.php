@extends('homepage.layout.default')
@section('title', 'Contact Us')
@section('content')

<section id="contact-info">
  <!-- <div class="center">
    <h2>How to Reach Us?</h2>
    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
  </div> -->
  <div class="gmap-area">
    <div class="container">
      <div class="row">
        <div class="col-sm-5 text-center">
          <div class="gmap">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.2133518191113!2d106.5820423142095!3d-6.235583995486271!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69fc3283577173%3A0x2a967fb1e5bc8a65!2sJl.+Raya+Binong+No.6%2C+RT.9%2FRW.1%2C+Binong%2C+Curug%2C+Tangerang%2C+Banten+15810!5e0!3m2!1sen!2sid!4v1555667186494!5m2!1sen!2sid" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div>

        <div class="col-sm-5 ml6 map-content">
          <ul class="row">
            <li class="col-sm-10">
              <address>
                <h5>Head Office</h5>
                <p>Ruko Aryana Karawaci Blok AA No 6. Jl Raya Binong, Karawaci - Kota Tangerang <br>
                </p>
                <p>Phone : 021-861-9856<br>
                <p>Mobile : 0813-8959-6455<br>
                <p>Email : sales@geoteksaintifik.com</p>
              </address>

              <!-- <address>
                <h5>Zonal Office</h5>
                <p>1537 Flint Street <br>
                Tumon, MP 96911</p>
                <p>Phone:670-898-2847 <br>
                Email Address:info@domain.com</p>
              </address> -->
            </li>


            <!-- <li class="col-sm-6">
              <address>
                <h5>Zone#2 Office</h5>
                <p>1537 Flint Street <br>
                Tumon, MP 96911</p>
                <p>Phone:670-898-2847 <br>
                Email Address:info@domain.com</p>
              </address>

              <address>
                <h5>Zone#3 Office</h5>
                <p>1537 Flint Street <br>
                Tumon, MP 96911</p>
                <p>Phone:670-898-2847 <br>
                Email Address:info@domain.com</p>
              </address>
            </li> -->
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>  <!--/gmap_area -->
@endsection
