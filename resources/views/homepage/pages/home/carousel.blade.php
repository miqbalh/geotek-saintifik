<section id="main-slider" class="no-margin">
  <div class="carousel slide">
    <ol class="carousel-indicators">
      @foreach ($collection as $key => $item)
      <li data-target="#main-slider" data-slide-to="{{ $key }}" class="@if($key === 0) active @endif"></li>
      @endforeach
    </ol>

    <div class="carousel-inner">
      <div style="background-image: url(template/images/slider/bc-carrousel.jpg)">

        @foreach ($collection as $key => $item)
          <div class="item @if($key === 0) active @endif">
            <div class="container">
              <div class="row slide-margin">
                <div class="col-sm-6">
                  <div class="carousel-content">
                    <h1 class="animation animated-item-1">{{ $item->title }}</h1>

                    @foreach ($category as $value)
                      @if ($value->id == $item->category)
                        @php $slug = $value->slug @endphp
                      @endif
                    @endforeach

                    <a class="btn-slide animation animated-item-3" href="{{ '/product/'. $slug .'/'. $item->slug }}">Read More</a>
                  </div>
                </div>

                <div class="col-sm-6 hidden-xs animation animated-item-4">
                  <div class="slider-img">
                    <img src="{{ asset(env('PATH_POST') .'/'.  $item->image) }}" class="img-responsive">
                  </div>
                </div>

              </div>
            </div>
          </div><!--/.item-->
        @endforeach

      </div>
    </div><!--/.carousel-inner-->
  </div><!--/.carousel-->
  <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
    <i class="fa fa-chevron-left"></i>
  </a>
  <a class="next hidden-xs" href="#main-slider" data-slide="next">
    <i class="fa fa-chevron-right"></i>
  </a>
</section><!--/#main-slider-->