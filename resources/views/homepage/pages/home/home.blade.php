@extends('homepage.layout.default')
@section('title', 'Home')
@section('content')
  @include('homepage.pages.home.carousel')

  <section id="feature" >
    <div class="container">
        <div class="center wow fadeInDown">
        <h2>PT. GEOTEK SAINTIFIK INDONESIA</h2>
        <p class="lead">PT. GEOTEK SANTIFIK INDONESIA was established in 2015 This company is engaged in services and Industrial Services, especially in the field of Laboratory Equipment. At present we are building a strong network in all Indonesian agencies including the Private Industry and Government sectors, Education and Research Centers and the Commercial sector.</p>
        <a class="btn-more" href="{{ URL('/about-us') }}">Read More</a>
    </div>
    </div><!--/.container-->
  </section><!--/#feature-->

  <section id="text-parners">
    <div class="container">
      <div class="center wow fadeInDown">
        <h2>Our Partners</h2>
        <!-- <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p> -->
      </div>

      <div class="partners">
        <ul>
          <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms" src="template/images/partners/QUALITEST-LOGO.jpg"></a></li>
          <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms" src="template/images/partners/TORONTECH -LOGO.jpg"></a></li>
          <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms" src="template/images/partners/Fuyi.png" width="50%"></a></li>
        </ul>
      </div>
    </div><!--/.container-->

  </section><!--/#partner-->
  <div class="gmap-area">
    <div class="container">
    <div class="center">
    <h2>How to Reach Us?</h2>
    </div>
      <div class="row">
        <div class="col-sm-5 text-center">
          <div class="gmap">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.2133518191113!2d106.5820423142095!3d-6.235583995486271!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69fc3283577173%3A0x2a967fb1e5bc8a65!2sJl.+Raya+Binong+No.6%2C+RT.9%2FRW.1%2C+Binong%2C+Curug%2C+Tangerang%2C+Banten+15810!5e0!3m2!1sen!2sid!4v1555667186494!5m2!1sen!2sid" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div>

        <div class="col-sm-5 ml6 map-content">
          <ul class="row">
            <li class="col-sm-10">
              <address>
                <h5>Head Office</h5>
                <p>Ruko Aryana Karawaci Blok AA No 6. Jl Raya Binong, Karawaci - Kota Tangerang <br>
                </p>
                <p>Phone : 021-861-9856<br>
                <p>Mobile : 0813-8959-6455<br>
                <p>Email : sales@geoteksaintifik.com</p>
              </address>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>  <!--/gmap_area -->
@endsection