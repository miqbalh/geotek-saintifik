<div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
  <a class="navbar-brand" href="index.html"><img src="{{ asset('template/images/logo/logo-text-small.png') }}" alt="logo"></a>
</div>

<div class="collapse navbar-collapse navbar-right">
  <ul class="nav navbar-nav">
    <li class="@if(Request::segment(1) == '') active @endif">
      <a href="{{ URL('/') }}">Home</a>
    </li>

    <li class="@if(Request::segment(1) == 'about-us') active @endif">
      <a href="{{ URL('/about-us') }}">About Us</a>
    </li>

    <!-- <li class="@if(Request::segment(1) == 'service') active @endif">
      <a href="{{ URL('/service') }}">Services</a>
    </li> -->

    <li class="dropdown @if(Request::segment(1) == 'blog-item') active @endif">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Products <i class="fa fa-angle-down"></i></a>
      <ul class="dropdown-menu">
        @foreach (fetch_category() as $item)
          <li><a href="{{ URL('product/'. $item->slug) }}">{{ $item->name }}</a></li>
        @endforeach
      </ul>
    </li>

    <li class="@if(Request::segment(1) == 'blog') active @endif">
      <a href="{{ URL('/customer') }}">Customers</a>
    </li>

    <li class="@if(Request::segment(1) == 'contact-us') active @endif">
      <a href="{{ URL('/contact-us') }}">Contact</a>
    </li>
  </ul>
</div>