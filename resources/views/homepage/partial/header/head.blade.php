<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>@yield('title')</title>

<!-- core CSS -->
<link href="{{ asset('template/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('template/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('template/css/animate.min.css') }}" rel="stylesheet">
<link href="{{ asset('template/css/prettyPhoto.css') }}" rel="stylesheet">
<link href="{{ asset('template/css/main.css?v=0.0.1') }}" rel="stylesheet">
<link href="{{ asset('template/css/responsive.css') }}" rel="stylesheet">
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->
<link rel="shortcut icon" href="{{ asset('template/images/logo/logo.png') }}">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
<link rel="stylesheet" href="https://unpkg.com/tachyons@4.10.0/css/tachyons.min.css"/>