<div class="col-sm-6">
  &copy; {{ date("Y") }} <a>Geotek Saintifik Indonesia</a>. All Rights Reserved.
</div>
<div class="col-sm-6">
  <ul class="pull-right">
    <li><a href="#">Home</a></li>
    <li><a href="{{ URL('/about-us') }}">About Us</a></li>
    <li><a href="{{ URL('/contact-us') }}">Contact Us</a></li>
  </ul>
</div>