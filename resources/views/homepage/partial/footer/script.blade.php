<script src="{{ asset('template/js/jquery.js') }}"></script>
<script src="{{ asset('template/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/js/jquery.prettyPhoto.js') }}"></script>
<script src="{{ asset('template/js/jquery.isotope.min.js') }}"></script>
<script src="{{ asset('template/js/main.js') }}"></script>
<script src="{{ asset('template/js/wow.min.js') }}"></script>