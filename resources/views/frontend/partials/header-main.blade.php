<div class="navbar navbar-main">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle">
        <i class="fa fa-bars"></i>
      </button>
      <a id="logo-geotek" class="navbar-brand" href="{{ route('home') }}">
        <img class="logo-default" src="{{ URL('/images/large-logo.png') }}" alt="logo" />
        <img class="logo-retina" src="{{ URL('/images/large-logo.png') }}" alt="logo" />
      </a>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="dropdown {{ isMenuActive('home') }}">
          <a href="{{ route('home') }}" role="button">Home</a>
        </li>

        <li class="dropdown {{ isMenuActive('category') }}">
          <a href="{{ route('category') }}" role="button">Category</a>
        </li>

        <li class="dropdown {{ isMenuActive('about') }}">
          <a href="{{ route('about') }}" role="button">About</a>
        </li>

        <li class="dropdown {{ isMenuActive('contact-us') }}">
          <a href="{{ route('contact-us') }}" role="button">Contact Us</a>
        </li>
      </ul>

      <div class="nav navbar-nav navbar-right">
        <div class="search-box-menu">
          <div class="search-box scrolldown">
            <input type="text" class="form-control" placeholder="Search for...">
          </div>
          <button type="button" class="btn btn-default btn-search">
            <span class="fa fa-search"></span>
          </button>
        </div>
        <ul class="nav navbar-nav lan-menu">
          <li class="dropdown">
            {{-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"><img src="{{ URL('/frontend/images/en.png') }}" alt="" />En<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="#"><img src="{{ URL('/frontend/images/it.png') }}" alt="" />IT</a></li>
            </ul> --}}
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>

@push('css-body')
<style>
#logo-geotek {
  height: 100px !important;
}
</style>
@endpush
