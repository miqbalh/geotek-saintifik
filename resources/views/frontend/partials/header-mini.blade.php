<div class="navbar-mini scroll-hide">
  <div class="container">
    <div class="nav navbar-nav navbar-left">
      <span><i class="fa fa-phone"></i>+62-21-59896701</span>
      <hr />
      <span><i class="fa fa-envelope"></i>sales@geoteksaintifik.com</span>
    </div>
    <div class="nav navbar-nav navbar-right">
      <div class="minisocial-group">
        {{-- <a target="_blank" href="#"><i class="fa fa-facebook first"></i></a>
        <a target="_blank" href="#"><i class="fa fa-instagram"></i></a>
        <a target="_blank" href="#"><i class="fa fa-youtube"></i></a>
        <a target="_blank" href="#"><i class="fa fa-linkedin"></i></a> --}}
      </div>
    </div>
  </div>
</div>

@push('css-body')
<style>
.navbar-mini i.fa.fa-phone {
  padding-left: .5rem;
}

.navbar-mini .nav.navbar-nav.navbar-left {
  margin-bottom: .5rem;
}
</style>
@endpush
