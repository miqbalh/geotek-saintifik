<div id="header-title" class="navbar navbar-main">
  <div class="container">
    <div class="navbar-header">
      {{-- <button type="button" class="navbar-toggle">
        <i class="fa fa-bars"></i>
      </button> --}}
      <a class="navbar-brand" href="{{ route('home') }}">
        <img class="logo-default" src="{{ URL('/images/large-logo.png') }}" alt="logo" />
        <img class="logo-retina" src="{{ URL('/images/large-logo.png') }}" alt="logo" />
      </a>
    </div>

    <form action="{{ route('search') }}" method="GET">
    <div
      class="search-box-menu"
      style="
        position: absolute;
        right: 2rem;
        top: 2rem;
    ">
      <input type="text" class="form-control" name="name" value="{{ app('request')->input('name') }}" placeholder="Search for...">
      <span
        class="fa fa-search"
        style="
          top: 1rem;
          right: 1rem;
          position: absolute;
      "></span>
    </div>
    </form>
  </div>
</div>

@push('css-body')
<style>
#header-title .navbar-collapse .navbar-nav.navbar-right {
  padding-top: 17px;
}
#header-title .navbar-collapse .search-box-menu {
  position: relative;
}
#header-title .navbar-collapse .search-box-menu span.fa.fa-search {
  position: absolute;
  top: 10px;
  right: 25px;
}

#header-title .navbar-header .navbar-brand {
  height: 85px !important;
}
</style>
@endpush
