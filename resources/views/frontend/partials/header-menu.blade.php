<div id="header-menu" class="navbar navbar-main">
  <div class="container">
    <div class="navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="dropdown {{ isMenuActive('home') }}">
          <a href="{{ route('home') }}" role="button">Home</a>
        </li>

        <li class="dropdown {{ isMenuActive('category') }}">
          <a href="{{ route('category') }}" role="button">Category</a>
        </li>

        <li class="dropdown {{ isMenuActive('about') }}">
          <a href="{{ route('about') }}" role="button">About</a>
        </li>

        <li class="dropdown {{ isMenuActive('contact-us') }}">
          <a href="{{ route('contact-us') }}" role="button">Contact Us</a>
        </li>
      </ul>
    </div>
  </div>
</div>

@push('css-body')
<style>
#header-menu {
  background-color: #FF0000;
}

#header-menu .navbar-collapse .navbar-nav {
  justify-content: center;
  display: flex;
  float: none;
}
#header-menu .navbar-collapse ul li a {
  color: white;
  text-transform: uppercase;
}
</style>
@endpush
