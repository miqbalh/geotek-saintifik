<div class="row">
  <div class="col-md-9">
    <div class="title-base text-left">
      <h1>Portfolio single three</h1>
      <p>Success is no accident. It is hard work, perseverance, studying, sacrifice and most of all, love of what you are doing or learning to do.</p>
    </div>
  </div>
  <div class="col-md-3">
    <ol class="breadcrumb b white">
      <li><a href="#">Home</a></li>
      <li><a href="#">Pages</a></li>
      <li class="active">Portfolio</li>
    </ol>
  </div>
</div>
