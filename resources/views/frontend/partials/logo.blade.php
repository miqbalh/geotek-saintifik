<div class="section-empty">
  <div class="container content text-center">
    @foreach ($category as $item)
      <a href="{{ $item->url }}">
        <img
          src="{{ asset(env('PATH_CATEGORY') .'/'.  $item->image) }}"
          alt="{{ $item->name }}"
          style="
            width: 13%;
            height: 100px;
            object-fit: contain;
          "
        >
      </a>
    @endforeach
  </div>
</div>
