<link rel="stylesheet" href="{{ asset('/frontend/scripts/font-awesome/css/font-awesome.css') }}">

<script async src="{{ asset('/frontend/scripts/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/frontend/scripts/imagesloaded.min.js') }}"></script>
<script src="{{ asset('/frontend/scripts/isotope.min.js') }}"></script>
<script src="{{ asset('/frontend/scripts/jquery.twbsPagination.min.js') }}"></script>
<script src="{{ asset('/frontend/scripts/flexslider/jquery.flexslider-min.js') }}"></script>
<script src="{{ asset('/frontend/scripts/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('/frontend/scripts/jquery.tab-accordion.js') }}"></script>
<script src="{{ asset('/frontend/scripts/bootstrap/js/bootstrap.popover.min.js') }}"></script>
<script src="{{ asset('/frontend/scripts/parallax.min.js') }}"></script>
<script src="{{ asset('/frontend/scripts/smooth.scroll.min.js') }}"></script>
<script src="{{ asset('/frontend/scripts/php/contact-form.js') }}"></script>
<script src="{{ asset('/frontend/scripts/jquery.progress-counter.js') }}"></script>
<script src="{{ asset('/frontend/scripts/social.stream.min.js') }}"></script>
<script src="{{ asset('/frontend/scripts/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('/frontend/scripts/google.maps.min.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
