<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
{{-- <meta name="viewport" content="width=device-width, initial-scale=1"> --}}

<!-- Standard -->
<title>{{ @$og_title ? $og_title . ' | PT GEOTEK SAINTIFIK' : 'PT GEOTEK SAINTIFIK | Scientific. Laboratory Equipment & Chemical' }}</title>

<meta name="description" content="{{ $og_description ?? 'Scientific. Laboratory Equipment & Chemical' }}">
<meta name="author" content="PT GEOTEK SAINTIFIK">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="canonical" href="{{ $og_url ?? 'http://www.geoteksaintifik.com/' }}">

<!-- Twitter Card -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="{{ $og_title ?? 'PT GEOTEK SAINTIFIK' }}">

<!-- OpenGraph -->
<meta property="og:title" content="{{ $og_title ?? 'PT GEOTEK SAINTIFIK' }}">
<meta property="og:description" content="{{ $og_description ?? 'Scientific. Laboratory Equipment & Chemical' }}">
<meta property="og:type" content="website">
<meta property="og:url" content="{{ $og_url ?? 'http://www.geoteksaintifik.com/' }}">
<meta property="og:site_name" content="PT GEOTEK SAINTIFIK">
<meta property="og:image" content="{{ $og_image ?? URL('/images/small-logo.jpeg') }}">
<meta property="og:locale" content="id_ID">

<link rel="icon" href="{{ URL('/images/small-logo.jpeg') }}">
