@extends('frontend.layout.default')

<!-- HEADER -->
@section('content-header')

@endsection

<!-- SIDEBAR -->
@section('content-sidebar')
@include('frontend.components.product-sidebar', [
  'categories' => $category,
])
@endsection

@section('content')
<div class="col-xs-9">
  @php
    $breadcrumbs = [
      (object) array(
        'name' => 'Product',
        'url' => route('category'),
      ),
      (object) array(
        'name' => @$products[0]->categorydata->name,
        'url' => '#',
      ),
    ];
  @endphp
  @include('frontend.components.simple-breadcrumb', [
    'title' => 'Product by Category',
    'breadcrumbs' => $breadcrumbs,
  ])

  @include('frontend.components.products', [
    'products' => $products,
  ])
</div>
@stop
