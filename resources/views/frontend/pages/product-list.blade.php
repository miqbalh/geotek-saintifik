@extends('frontend.layout.default')

<!-- SIDEBAR -->
@section('content-sidebar')
@include('frontend.components.product-sidebar', [
  'categories' => $category,
])
@endsection

@section('content')
@include('frontend.components.category', [
  'categories' => $category,
])
@stop
