@extends('frontend.layout.default')

<!-- HEADER -->
@section('content-header')

@endsection

<!-- SIDEBAR -->
@section('content-sidebar')
@include('frontend.components.product-sidebar', [
  'categories' => $category,
])
@endsection

@section('content')

<div id="categories" class="col-xs-8">
  @php
    $breadcrumbs = [
      (object) array(
        'name' => 'Product',
        'url' => '#',
      ),
    ];
  @endphp
  @include('frontend.components.simple-breadcrumb', [
    'title' => 'All Category',
    'breadcrumbs' => $breadcrumbs,
  ])

  @foreach ($groupCategory as $subCategory)
  @if(count($subCategory->products) > 0)

  <div class="row category-title">
    <div class="col-12">
      <div class="title-base text-left">
        <hr>
        <h2>{{ $subCategory->name }}</h2>
      </div>
    </div>
  </div>

  <div class="row category-item">
    <div class="col-12">
      <ul class="ul-squares">
        @foreach ($subCategory->products as $product)
        <li>
          <a href="{{ $product->url }}">
            {{ $product->title }}
          </a>
        </li>
        @endforeach
      </ul>
    </div>
  </div>

  @endif
  @endforeach
</div>

@stop

@push('css')
<style>
#categories {
  margin-left: 8.333333%;
}

#categories ul {
  column-count: 2;
}

#categories .category-item {
  margin-bottom: 2rem;
}
</style>
@endpush
