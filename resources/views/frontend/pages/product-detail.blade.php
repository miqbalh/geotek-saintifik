@extends('frontend.layout.default')

<!-- HEADER -->
@section('content-header')

@endsection

<!-- SIDEBAR -->
@section('content-sidebar')
@include('frontend.components.product-sidebar', [
  'categories' => $category,
])
@endsection

@section('content')
<div class="col-xs-9">
  @php
    $breadcrumbs = [
      (object) array(
        'name' => 'Product',
        'url' => route('category'),
      ),
      (object) array(
        'name' => @$product->categorydata->name,
        'url' => route('list-category', [
            'category' => @$product->categorydata->slug
        ]),
      ),
      (object) array(
        'name' => $product->title,
        'url' => '#',
      ),
    ];
  @endphp

  @include('frontend.components.simple-breadcrumb', [
    'title' => 'Detail Product',
    'breadcrumbs' => $breadcrumbs,
  ])

  <div id="detail-product">
    <div class="row">
      <div class="col-md-12 text-center">
        <img
          src="{{ asset(env('PATH_POST') .'/'.  $product->image) }}"
          alt="{{ $product->title }}"
          class="product-cover"
        />
        <hr class="space s" />
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <h3 class="text-normal">Details</h3>
        <ul class="list-texts list-texts-justified">
          <li><b>Name:</b> <span>{{ @$product->title }}</span></li>
          <li><b>Category:</b> <span>{{ @$product->categorydata->name }}</span></li>
        </ul>
        <hr class="space s" />
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <h3 class="text-normal">Description</h3>
        <p>{!! $product->description !!}</p>
      </div>
    </div>
  </div>
</div>
@stop

@push('css')
<style>
#detail-product .product-cover {
  margin-bottom: 5rem;
}
</style>
@endpush
