@extends('frontend.layout.default')

<!-- HEADER -->
@section('content-header')
@php
  $breadcrumbs = [
  ];
@endphp

@include('frontend.components.breadcrumb', [
  'title' => 'Contact Us',
  'breadcrumbs' => $breadcrumbs,
])
@endsection

@section('content')

<div class="col-md-10 col-center text-center">
  <!-- MAP -->
  <iframe
  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.2133518191113!2d106.5820423142095!3d-6.235583995486271!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69fc3283577173%3A0x2a967fb1e5bc8a65!2sJl.+Raya+Binong+No.6%2C+RT.9%2FRW.1%2C+Binong%2C+Curug%2C+Tangerang%2C+Banten+15810!5e0!3m2!1sen!2sid!4v1555667186494!5m2!1sen!2sid"
  width="1000"
  height="450"
  frameborder="0"
  style="border:0"
  allowfullscreen
  ></iframe>
  <hr class="space">

  <!-- ADDRESS -->
  <h2 class="">How to Contact Us</h2>
  <hr class="space s">
  <h4 class="text-color no-margins">
    Head Office
  </h4>
  <p class="">
    Ruko Aryana Karawaci Blok AA No 6. Jl Raya Binong, <br>
    Karawaci - Kota Tangerang<br>
    +62-21-59896701<br>
    sales@geoteksaintifik.com
  </p>
  <hr class="space m">

  @if (@session('success_message'))
  <div class="success-box">
    <div class="alert alert-success">
      {{ @session('success_message') }}
    </div>
  </div>
  @endif

  @if (@$error_message)
  <div class="error-box">
    <div class="alert alert-warning">
      {{ @$error_message }}
    </div>
  </div>
  @endif


  <!-- FORM -->
  <form action="{{ route('contact-us-post') }}" class="form-box" method="POST">
    @csrf

    <div class="row">
      <div class="col-md-6">
        <p>Your name</p>
        <input
          id="name"
          name="name"
          placeholder=""
          value="{{ @$input['name'] }}"
          type="text"
          class="form-control form-value"
          required=""
        >
      </div>

      <div class="col-md-6">
        <p>Your Email Address</p>
        <input
          id="email"
          name="email"
          placeholder=""
          type="email"
          value="{{ @$input['email'] }}"
          class="form-control form-value"
          required=""
        >
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <p>Your Company Name</p>
        <input
          id="company_name"
          name="company_name"
          placeholder=""
          value="{{ @$input['company_name'] }}"
          type="text"
          class="form-control form-value"
        >
      </div>

      <div class="col-md-6">
        <p>Your Contact Number</p>
        <input
          id="phone"
          name="phone"
          placeholder=""
          value="{{ @$input['phone'] }}"
          type="text"
          class="form-control form-value"
        >
      </div>
    </div>

    <hr class="space xs">
    <div class="row">
      <div class="col-md-12">
        <p>Your message</p>
        <textarea
          id="messagge"
          name="message"
          placeholder=""
          class="form-control form-value"
          required=""
        >{{ @$input['message'] }}</textarea>
        <hr class="space s">
        <button type="submit" class="anima-button btn-sm btn">
          <i class="fa fa-send-o"></i>Send messagge</a>
        </button>
      </div>
    </div>
  </form>
</div>

@stop

