@extends('frontend.layout.default')

@section('content')

@include('frontend.components.home.carousel', ['collection' => $collection])

@include('frontend.components.home.about')

@include('frontend.components.home.partners', ['categories' => $category])

@include('frontend.components.home.map')

@stop
