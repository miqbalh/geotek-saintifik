@extends('frontend.layout.default')

<!-- HEADER -->
@section('content-header')
@php
  $breadcrumbs = [
  ];
@endphp

@include('frontend.components.breadcrumb', [
  'title' => 'About',
  'breadcrumbs' => $breadcrumbs,
])
@endsection

@section('content')

<div class="col-md-8 col-center text-center">
  <!-- TITLE -->
  <img src="{{ URL('/images/large-logo.png') }}" alt="PT. GEOTEK SANTIFIK INDONESIA">
  <hr class="space m">

  <!-- DESCRIPTION -->
  <p>PT. GEOTEK SANTIFIK INDONESIA was established in 2015 This company is engaged in services and Industrial Services, especially in the field of Laboratory Equipment. At present we are building a strong network in all Indonesian agencies including the Private Industry and Government sectors, Education and Research Centers and the Commercial sector.</p>
  <p>Our company is building a good networking with customers to get quality products that suit the needs of each customer.</p>
  <p>Always rely on the quality and innovate in terms of marketing products to ensure that these products are products that are in accordance with customer needs At present we are establishing good relationships with imported products, namely the best producers in the world to promote and sell high-tech products to facilitate and assist each customer in terms of research. and provide the best after-sales service to all our customers.</p>
  <hr class="space m">
  <hr class="space m">

  <!-- VISION -->
  <h2 class="text-color">VISION</h2>
  <p class="block-quote quote-1 quote-gray text-center">PT. GEOTEK SAINTIFIK INDONESIA Having a vision to make this company the largest company in the field of laboratory equipment</p>
  <hr class="space m">
  <hr class="space m">

  <h2 class="text-color">MISION</h2>
  <p class="block-quote quote-1 quote-gray text-center">Doing work for customer satisfaction</p>
</div>

@stop
