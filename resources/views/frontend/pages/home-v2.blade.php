@extends('frontend.layout.default')

<!-- HEADER -->
@section('content-header')
@include('frontend.components.carousel', [
  'collection' => $banner,
])
@endsection

<!-- SIDEBAR -->
@section('content-sidebar')
@include('frontend.components.product-sidebar', [
  'categories' => $category,
])
@endsection

@section('content')
<div class="col-xs-9">
  @include('frontend.components.products', [
    'products' => $products,
  ])

  @include('frontend.components.home.partners', [
    'categories' => $category
  ])
</div>
@stop
