<div class="row simple-breadcrumb">
  <ol class="breadcrumb b text-left">
    @php
      $end = end($breadcrumbs);
    @endphp
    @foreach ($breadcrumbs as $key => $item)
      @if ($item == $end)
        <li class="active">
          {{ $item->name }}
        </li>
      @else
        <li>
          <a href="{{ $item->url }}">
            {{ $item->name }}
          </a>
        </li>
      @endif
    @endforeach
  </ol>
</div>
