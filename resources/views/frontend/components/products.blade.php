@if (count($products) > 1)
<div>
  <div class="row" data-lightbox-anima="show-scale">
    @foreach ($products as $key => $item)
      <div id="product-item" class="col-md-4">
      <div class="img-box adv-img adv-img-down-text">
        <a class="img-box" href="{{ $item->url }}">
          <div class="caption">
            <i class="fa fa-plus"></i>
          </div>

          <img
            src="{{ asset(env('PATH_POST') .'/'.  $item->image) }}"
            alt="{{ $item->title }}"
          />
        </a>

        <div class="caption-bottom">
          <h2>
            <a href="{{ $item->url }}">
              {{ $item->title }}
            </a>
          </h2>

          <div class="max-lenght">{!! $item->first_paragraph !!}</div>
        </div>
      </div>
    </div>
    @endforeach

    <div class="clear"></div>
  </div>

  <div class="list-nav">
    @include('frontend.components.pagination', [
      'paginations' => $products,
    ])
  </div>
</div>
@else
<div class="text-center">
  <h2>Product not found!!!</h2>
</div>
@endif

@push('css')
<style>
#product-item .adv-img-down-text h2 a {
  min-height: 50px;
  font-weight: 450;
  padding-bottom: 0px;
  padding-top: 0px;
  font-size: 15px;
  line-height: 1.6;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 2; /* number of lines to show */
  -webkit-box-orient: vertical;
}

#product-item .adv-img .img-box img {
  width: 100%;
  min-height: 250px;
  max-height: 250px;
  object-fit: scale-down;
}

#product-item .caption-bottom .max-lenght {
  min-height: 74px !important;
  max-height: 74px !important;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 4; /* number of lines to show */
  -webkit-box-orient: vertical;
}
</style>
@endpush
