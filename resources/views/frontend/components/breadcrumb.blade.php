<div class="header-base">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <div class="title-base text-left">
          <h1>{{ $title }}</h1>
          {{-- <p>The writer's curse is that even in solitude, no matter its duration, he never grows lonely or bored.</p> --}}
        </div>
      </div>
      <div class="col-md-3">
        <ol class="breadcrumb b white">
          @php
            $end = end($breadcrumbs);
          @endphp
          @foreach ($breadcrumbs as $key => $item)
            @if ($key == $end)
              <li class="active">{{ $item }}</li>
            @else
              <li><a>{{ $item }}</a></li>
            @endif
          @endforeach
        </ol>
      </div>
    </div>
  </div>
</div>
