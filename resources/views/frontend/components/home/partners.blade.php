<div class="section-empty" style="margin-top: 100px">
  <h1 class="text-center" style="margin-bottom: 25px;font-weight: 500;">Our Customers</h1>

  <div class="flexslider carousel" data-options="numItems:5,itemMargin:30,minWidth:100,maxWidth:100,controlNav:true,directionNav:true">
    <ul class="slides">

      @foreach ($customer as $item)
        @if($item->logo)
        <li>
          <div class="img-box adv-img adv-img-half-content" data-anima="fade-left" data-trigger="hover">
            <a href="#" class="img-box anima-scale-up anima text-center">
              <img src="{{ asset(env('PATH_CUSTOMER') .'/'.  $item->logo) }}" alt="{{ $item->name }}" style="height: 100px; width: auto;">
            </a>
          </div>
        </li>
        @endif
      @endforeach

    </ul>
  </div>
</div>

