<div class="section-map box-middle-container row-20">
  <div
    class="google-map"
    data-coords="-6.2505743,106.5721517"
    data-zoom="12"
    data-skin="gray"
    data-marker-pos-top="30"
    data-marker-pos="col-md-6-right"
  ></div>

  <div class="overlaybox overlaybox-side overlaybox">
    <div class="container content">
      <div class="row">
        <div class="col-md-6 overlaybox-inner box-middle" data-anima="fade-left">
          <div class="row">
            <div class="col-md-6">
              <h4 class="text-m text-normal">Yellow Business</h4>
              <p class="text-left">
                Ligula aenean, voluptatem a lorem laoreet quod dolores acnatoque modiignani merto inventore.
              </p>
              <hr class="space s" />
              <div class="btn-group btn-group-icons" role="group">
                <a class="btn btn-default">
                  <i class="fa fa-facebook"></i>
                </a>
                <a class="btn btn-default">
                  <i class="fa fa-twitter"></i>
                </a>
                <a class="btn btn-default">
                  <i class="fa fa-google-plus"></i>
                </a>
                <a class="btn btn-default">
                  <i class="fa fa-linkedin"></i>
                </a>
              </div>
            </div>
            <div class="col-md-6">
              <hr class="space visible-xs" />
              <h4 class="text-m text-normal">Contacts</h4>
              <p>
                05, Martin Street A598, Polo Alto,<br /> San Francisco
                United States of America
              </p>
              <ul class="fa-ul">
                <li><i class="fa-li fa-envelope fa"></i> hello@company.com</li>
                <li><i class="fa-li fa-phone-square fa"></i> (123) 456-78910</li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-6"></div>
      </div>
    </div>
  </div>
</div>
