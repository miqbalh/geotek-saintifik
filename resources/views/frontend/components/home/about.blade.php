<div class="section-empty">
  <div class="container content">
    <div class="row vefrtical-row">
      <div class="col-md-4">
        <div class="title-base  text-left">
          <hr />
          <h2>PT. Geotek Saintifik Indonesia</h2>
          <p>About Us</p>
        </div>
        <p><span class="text-color">PT. GEOTEK SANTIFIK INDONESIA</span> was established in 2015 This company is engaged in services and Industrial Services, especially in the field of Laboratory Equipment. At present we are building a strong network in all Indonesian agencies including the Private Industry and Government sectors, Education and Research Centers and the Commercial sector.</p>
      </div>
      <div class="col-md-4">
        <p>Our company is building a good networking with customers to get quality products that suit the needs of each customer.</p>
        <p>Always rely on the quality and innovate in terms of marketing products to ensure that these products are products that are in accordance with customer needs At present we are establishing good relationships with imported products, namely the best producers in the world to promote and sell high-tech products to facilitate and assist each customer in terms of research. and provide the best after-sales service to all our customers.</p>
      </div>
      <div class="col-md-4">
        <p class="block-quote quote-1">
          PT. GEOTEK SAINTIFIK INDONESIA Having a vision to make this company
          the largest company in the field of laboratory equipment
          <span class="quote-author">Vision</span>
        </p>
      </div>
    </div>
  </div>
</div>
