<div class="section-empty no-paddings">
  <div class="section-slider row-18">
    <div class="flexslider advanced-slider slider" data-options="animation:fade">
      <ul class="slides">

        @foreach ($collection as $item)
        @if($item->url_cover)
        <li data-slider-anima="fade-left" data-time="1000">
          <div class="section-slide text-center">
            <a href="{{ $item->url }}" >
              <img src="{{ $item->url_cover }}">
            </a>
          </div>
        </li>
        @endif
        @endforeach

      </ul>
    </div>
  </div>
</div>

@push('css')
<style>
.section-slider .slides .section-slide .max-lenght {
  min-height: 74px !important;
  max-height: 74px !important;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 4; /* number of lines to show */
  -webkit-box-orient: vertical;
}
</style>
@endpush
