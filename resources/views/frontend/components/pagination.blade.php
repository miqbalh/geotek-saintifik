@if ($paginations->lastPage() > 1)
<ul class="pagination pagination-lg pagination-grid">
  <!-- Previous -->
  @if($paginations->currentPage() != 1)
  <li class="prev">
    <a href="{{ $paginations->url(1) }}">
      <i class="fa fa-angle-left"></i>
      <span>Previous</span>
    </a>
  </li>
  @endif

  @for ($i = 1; $i <= $paginations->lastPage(); $i++)
  <li class="
    page
    @if($paginations->currentPage() == $i) active @endif
  ">
    <a href="{{ $paginations->url($i) }}">
      {{ $i }}
    </a>
  </li>
  @endfor

  <!-- Next -->
  @if($paginations->currentPage() != $paginations->lastPage())
  <li class="next">
    <a href="{{ $paginations->url($paginations->currentPage()+1) }}">
      <span>Next</span>
      <i class="fa fa-angle-right"></i>
    </a>
  </li>
  @endif
</ul>
@endif

@push('css')
<style>
.pagination.pagination-lg.pagination-grid {
  margin-top: 5rem !important;
}
</style>
@endpush
