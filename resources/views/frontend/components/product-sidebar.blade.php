<div id="product-sidebar" class="col-xs-3 widget">
  {{-- <div class="input-group search-blog list-blog">
    <input type="text" class="form-control" placeholder="Search for...">
    <span class="input-group-btn">
      <button class="btn btn-default" type="button">Go!</button>
    </span>
  </div>

  <hr class="space s" /> --}}

  <div class="list-group latest-post-list list-blog">
    <p class="list-group-item active list-title">Product</p>

    @foreach ($categories as $item)
    <div class="list-group-item" style="padding: 0">
      <div class="row">
        <div class="col-md-12 text-center">
          <a href="{{ $item->url }}">
            <img
              src="{{ asset(env('PATH_CATEGORY') .'/'.  $item->image) }}"
              alt="{{ $item->name }}"
              height="100px"
            >
          </a>
        </div>
      </div>
    </div>
    @endforeach

  </div>
</div>

@push('css-body')
<style>
#product-sidebar .list-group.list-blog .list-title {
  margin: 0;
  padding: 7px 0;
  background-color: #FF0000;
  color: #FFFFFF;
  text-align: center;
}
#product-sidebar .list-group.list-blog .list-group-item {
  border: 2px solid #333;
}
</style>
@endpush
