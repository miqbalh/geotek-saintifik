<!DOCTYPE html>
<!--[if lt IE 10]> <html  lang="en" class="iex"> <![endif]-->
<!--[if (gt IE 10)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head>
  @include('frontend.partials.head.meta')

  @include('frontend.partials.head.style')

  @include('frontend.partials.head.script')

  @stack('css')
</head>
<body>
  <div id="preloader"></div>

  <header class="fixed-top scroll-change" data-menu-anima="fade-in">
    <div class="navbar navbar-default mega-menu-fullwidth navbar-fixed-top" role="navigation">
      {{-- @include('frontend.partials.header-mini') --}}

      {{-- @include('frontend.partials.header-main') --}}

      @include('frontend.partials.header-title')

      @include('frontend.partials.header-menu')
    </div>
  </header>

  <!-- HEADER -->
  @yield('content-header')

  <div class="section-empty section-item">
    <div class="container content">
      <div class="row">
        <!-- SIDEBAR -->
        @yield('content-sidebar')

        <!-- CONTENT -->
        @yield('content')
      </div>
    </div>
  </div>

  @include('frontend.partials.logo')

  <i class="scroll-top scroll-top-mobile show fa fa-sort-asc"></i>

  <footer class="footer-base">
    @stack('css-body')

    @include('frontend.partials.foot.footer')

    @include('frontend.partials.foot.script')
  </footer>
</body>
</html>
