@extends('dashboard.layout.default')

@section('title', 'Post')

@section('content')

@if (@$error_message)
<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h5><i class="icon fa fa-ban"></i> Alert!</h5>
  {{ @$error_message }}
</div>
@endif

<div class="box box-info">
  <div class="box-header with-border">
    <h3 class="box-title">Update Post</h3>
  </div>
  <form action="{{ URL('admin/post/update', $form['id']) }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="box-body">
      <div class="form-group">
        <label class="col-sm-2 control-label">Title</label>

        <div class="col-sm-10">
          <input type="text" class="form-control" name="title" placeholder="Title"
            value="{{ @$input['title'] ? @$input['title'] : $form['title'] }}">
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-2 control-label">Slug</label>

        <div class="col-sm-10">
          <input type="text" class="form-control" name="slug" placeholder="Slug"
            value="{{ @$input['slug'] ? @$input['slug'] : $form['slug'] }}">
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-2 control-label">Category</label>

        <div class="col-sm-10">
          <select class="form-control select2" name="category" style="width: 100%;">
            @php $value = @$input['category'] ? @$input['category'] : $form['category'] @endphp
            @foreach(@$category as $item)
              <option @if(@$value == $item->id) selected @endif value="{{ $item->id }}">{{ $item->name }}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-2 control-label">Description</label>

        <div class="col-sm-10">
          <textarea id="editor1" name="description">
            {{ @$input['description'] ? @$input['description'] : $form['description'] }}
          </textarea>
        </div>
      </div> <br/>

      <div class="form-group">
        <label class="col-sm-2 control-label">Image</label>
        <div class="col-sm-10">
          <input type="file" name="image">
          <code>Max 1mb | Format JPG, JPEG, atau PNG.</code>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-2 control-label">Image Cover</label>
        <div class="col-sm-10">
          <input type="file" name="cover" accept="image/*">
          <code>Max 1mb | Format JPG, JPEG, atau PNG.</code>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-2 control-label">Is Homepage</label>
        <div class="col-sm-10">
          <select class="form-control" name="is_homepage">
            @php $value = @$input['is_homepage'] ? @$input['is_homepage'] : $form['is_homepage'] @endphp
            <option @if($value == "0") selected @endif value="0">No</option>
            <option @if($value == "1") selected @endif value="1">Yes</option>
          </select>
        </div>
      </div>
    </div>

    <div class="box-footer">
      <a href="{{ URL('/admin/post') }}"><button type="button" class="btn btn-default">Cancel</button></a>
      <button type="submit" class="btn btn-primary pull-right">Save</button>
    </div>
  </form>
</div>

@endsection
