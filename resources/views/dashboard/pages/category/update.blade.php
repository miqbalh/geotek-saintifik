@extends('dashboard.layout.default')

@section('title', 'Category')

@section('content')

@if (@$error_message)
<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h5><i class="icon fa fa-ban"></i> Alert!</h5>
  {{ @$error_message }}
</div>
@endif

<div class="box box-info">
  <div class="box-header with-border">
    <h3 class="box-title">Update Category</h3>
  </div>
  <form action="{{ URL('admin/category/update', $form['id']) }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="box-body">
      <div class="form-group">
        <label class="col-sm-2 control-label">Name</label>

        <div class="col-sm-10">
          <input type="text" class="form-control" name="name" placeholder="Name"
            value="{{ @$input['name'] ? @$input['name'] : $form['name'] }}">
        </div>
      </div>

      {{-- <div class="form-group">
        <label class="col-sm-2 control-label">Parent</label>

        <div class="col-sm-10">
          <select class="form-control select2" name="parent" style="width: 100%;">
            @php $value = @$input['parent'] ? @$input['parent'] : $form['parent'] @endphp

            <option @if(@$value == '0') selected @endif value="0">root</option>
            @foreach(@$category as $item)
              <option @if(@$value == $item->id) selected @endif value="{{ $item->id }}">{{ $item->name }}</option>
            @endforeach
          </select>
        </div>
      </div> --}}

      <div class="form-group">
        <label class="col-sm-2 control-label">Image</label>
        <div class="col-sm-10">
          <input type="file" name="image">
          <code>Max 1mb | Format JPG, JPEG, atau PNG.</code>
        </div>
      </div>
    </div>

    <div class="box-footer">
      <a href="{{ URL('/admin/category') }}"><button type="button" class="btn btn-default">Cancel</button></a>
      <button type="submit" class="btn btn-primary pull-right">Save</button>
    </div>
  </form>
</div>

@endsection
