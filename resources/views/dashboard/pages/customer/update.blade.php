@extends('dashboard.layout.default')

@section('title', 'Customer')

@section('content')

@if (@$error_message)
<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h5><i class="icon fa fa-ban"></i> Alert!</h5>
  {{ @$error_message }}
</div>
@endif

<div class="box box-info">
  <div class="box-header with-border">
    <h3 class="box-title">Update Customer</h3>
  </div>
  <form action="{{ URL('admin/customer/update', $form['id']) }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="box-body">
      <div class="form-group">
        <label class="col-sm-2 control-label">Name</label>

        <div class="col-sm-10">
          <input type="text" class="form-control" name="name" placeholder="Name"
            value="{{ @$input['name'] ? @$input['name'] : $form['name'] }}">
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-2 control-label">Logo</label>
        <div class="col-sm-10">
          <input type="file" name="logo">
          <code>Max 1mb | Format JPG, JPEG, atau PNG.</code>
        </div>
      </div>
    </div>

    <div class="box-footer">
      <a href="{{ URL('/admin/customer') }}"><button type="button" class="btn btn-default">Cancel</button></a>
      <button type="submit" class="btn btn-primary pull-right">Save</button>
    </div>
  </form>
</div>

@endsection
