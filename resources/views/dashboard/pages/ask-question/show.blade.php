@extends('dashboard.layout.default')

@section('title', 'Customer')

@section('content')

<div class="box box-primary">
  <div class="box-body no-padding">
    <div class="mailbox-read-info">
      <h3>{{ $collection->name }}, {{ $collection->company_name }}</h3>
      <h5>From: {{ $collection->email }}
        <span class="mailbox-read-time pull-right">
          {{ $collection->full_date }}
        </span>
      </h5>
    </div>

    <div class="mailbox-read-message">
      {{ $collection->message }}
    </div>
  </div>

  <div class="box-footer">
    <a href="{{ URL('/admin/ask-question') }}">
      <button type="button" class="btn btn-default">
        Cancel
      </button>
    </a>
  </div>
</div>

@endsection
