@extends('dashboard.layout.default')

@section('title', 'Ask Questions')

@section('content')

@if (@session('success_message'))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h5><i class="icon fa fa-check"></i> Alert!</h5>
  {{ @session('success_message') }}
</div>
@endif

<div class="box box-primary">
  <div class="box-body">
    <div class="table-responsive mailbox-messages">
      <table id="example1" class="table table-hover table-striped">
        <thead class="mt-3">
          <tr>
            <th>Contact</th>
            <th>Message</th>
            <th>Date</th>
            <th></th>
          </tr>
        </thead>

        <tbody>
          @foreach ($collection as $item)
          <tr>
            <td style="width: 20%" class="mailbox-name">
              <a href="{{ URL('/admin/ask-question/'. $item->id) }}">
                <b>{{ $item->name }}</b>
              </a>
              | {{ $item->company_name }}
            </td>

            <td style="width: 70%" class="mailbox-subject">
              {{ $item->sort_message }}
            </td>

            <td style="width: 10%;" class="mailbox-date">
              {{ $item->simple_date }}
            </td>

            <td class="w-20">
              <a onclick="deleteRow('/admin/ask-question', {{ $item->id }})">
                <button class="btn btn-danger">
                  <i class="fa fa-trash"></i>
                  delete
                </button>
              </a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>



@endsection
