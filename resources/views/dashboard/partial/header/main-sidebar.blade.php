<aside class="main-sidebar">
  <section class="sidebar">
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{ asset('bower_components/admin-lte/dist/img/user4-128x128.jpg') }}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>Alexander Pierce</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    {{-- <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form> --}}
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li class="@if(Request::segment(2) == '') active @endif">
        <a href="{{ URL('/admin') }}">
          <i class="fa fa-dashboard"></i>
          <span>Dashboard</span>
        </a>
      </li>

      <li class="@if(Request::segment(2) == 'post') active @endif">
        <a href="{{ URL('/admin/post') }}">
          <i class="fa fa-sticky-note"></i>
          <span>Post</span>
        </a>
      </li>

      <li class="@if(Request::segment(2) == 'category') active @endif">
        <a href="{{ URL('/admin/category') }}">
          <i class="fa fa-navicon"></i>
          <span>Category</span>
        </a>
      </li>

      <li class="@if(Request::segment(2) == 'customer') active @endif">
        <a href="{{ URL('/admin/customer') }}">
          <i class="fa fa-users"></i>
          <span>Customer</span>
        </a>
      </li>

      <li class="@if(Request::segment(2) == 'ask-question') active @endif">
        <a href="{{ URL('/admin/ask-question') }}">
          <i class="fa fa-question"></i>
          <span>Ask Question</span>
        </a>
      </li>
    </ul>
  </section>
</aside>
