<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Homepage')->group(function () {

    /*
    |--------------------------------------------------------------------------
    | HOME PAGE
    |--------------------------------------------------------------------------
    */
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/about', 'HomeController@about')->name('about');


    /*
    |--------------------------------------------------------------------------
    | CUSTOMER PAGE
    |--------------------------------------------------------------------------
    */
    Route::get('/customer', 'CustomerController@index');

    /*
    |--------------------------------------------------------------------------
    | PRODUCT PAGE
    |--------------------------------------------------------------------------
    */
    Route::get('/category/', 'ProductController@index')->name('category');
    Route::get('/category/search', 'ProductController@search')->name('search');
    Route::get('/category/{category}', 'ProductController@productCategory')->name('list-category');
    Route::get('/category/{category}/{product}', 'ProductController@productDetail')->name('detail-product');

    /*
    |--------------------------------------------------------------------------
    | CONTACT PAGE
    |--------------------------------------------------------------------------
    */
    Route::get('/contact-us/', 'ContactController@index')->name('contact-us');
    Route::post('/contact-us/', 'ContactController@store')->name('contact-us-post');
});


Route::namespace('Dashboard')->group(function () {

    /*
    |--------------------------------------------------------------------------
    | LOGIN
    |--------------------------------------------------------------------------
    */
    Route::get('login', 'UserController@login');
    Route::post('login', 'UserController@login');


    Route::group(['middleware' => 'admin', 'prefix' => 'admin'], function(){

        Route::get('/', function () {
            return view('dashboard.pages.index');
        });

        /*
        |--------------------------------------------------------------------------
        | POST
        |--------------------------------------------------------------------------
        */
        Route::get('post', 'PostController@index');
        Route::post('post', 'PostController@index');
        Route::get('post/create/', 'PostController@create');
        Route::post('post/create/', 'PostController@create');
        Route::get('post/update/{id}', 'PostController@update');
        Route::post('post/update/{id}', 'PostController@update');
        Route::get('post/delete/{id}', 'PostController@destroy');

        /*
        |--------------------------------------------------------------------------
        | CATEGORY
        |--------------------------------------------------------------------------
        */
        Route::get('category', 'CategoryController@index');
        Route::post('category', 'CategoryController@index');
        Route::get('category/create/', 'CategoryController@create');
        Route::post('category/create/', 'CategoryController@create');
        Route::get('category/update/{id}', 'CategoryController@update');
        Route::post('category/update/{id}', 'CategoryController@update');
        Route::get('category/delete/{id}', 'CategoryController@destroy');

        /*
        |--------------------------------------------------------------------------
        | CUSTOMER
        |--------------------------------------------------------------------------
        */
        Route::get('customer', 'CustomerController@index');
        Route::post('customer', 'CustomerController@index');
        Route::get('customer/create/', 'CustomerController@create');
        Route::post('customer/create/', 'CustomerController@create');
        Route::get('customer/update/{id}', 'CustomerController@update');
        Route::post('customer/update/{id}', 'CustomerController@update');
        Route::get('customer/delete/{id}', 'CustomerController@destroy');

        /*
        |--------------------------------------------------------------------------
        | ASK QUESTIONS
        |--------------------------------------------------------------------------
        */
        Route::get('ask-question', 'AskQuestionController@index');
        Route::get('ask-question/{id}', 'AskQuestionController@show');
        Route::get('ask-question/read/{id}', 'AskQuestionController@read');
        Route::get('ask-question/unread/{id}', 'AskQuestionController@unread');
        Route::get('ask-question/delete/{id}', 'AskQuestionController@destroy');

        /*
        |--------------------------------------------------------------------------
        | LOGOUT
        |--------------------------------------------------------------------------
        */
        Route::get('logout', 'UserController@logout');
    });
});
