<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    /**
    * The attributes that are appends.
    *
    * @var array
    */
    protected $appends = [
        'url',
    ];

    /**
    * Product Url
    *
    * @return String
    */
    public function getUrlAttribute()
    {
        return route('list-category', [
            'category' => $this->slug
        ]);
    }
}
