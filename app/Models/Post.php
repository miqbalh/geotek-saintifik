<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [
        'categorydata',
    ];

    /**
     * Get the category for the product.
     */
    public function Categorydata()
    {
        return $this->hasOne('App\Models\Category', 'id', 'category');
    }

    /**
    * The attributes that are appends.
    *
    * @var array
    */
    protected $appends = [
        'url',
        'url_cover',
        'first_paragraph',
        'first_sentence',
    ];

    /**
    * Product Url
    *
    * @return String
    */
    public function getUrlAttribute()
    {
        $category_slug = @$this->categorydata->slug;

        return route('detail-product', [
            'category' => $category_slug,
            'product' => $this->slug
        ]);
    }

    /**
    * Product Url
    *
    * @return String
    */
    public function getUrlCoverAttribute()
    {
        if (!$this->cover) return '';

        return URL(env('PATH_POST_COVER') .'/'. $this->cover);
    }

    /**
    * Get first paragraph with tag <p>
    *
    * @return String
    */
    public function getFirstParagraphAttribute()
    {
        $html = $this->description;
        $start = strpos($html, '<p>');
        $end = strpos($html, '</p>', $start);
        return substr($html, $start, $end-$start+4);
    }

    /**
    * Get first sentance
    *
    * @return String
    */
    public function getFirstSentenceAttribute() {
        $sentance = strpos($this->description, '.');
        return substr($this->description, 0, $sentance+1);
    }
}
