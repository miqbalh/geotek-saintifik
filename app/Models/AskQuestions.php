<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AskQuestions extends Model
{
    use SoftDeletes;

    /**
    * The collection associated with the model.
    *
    * @var string
    */
    protected $collection = 'ask_questions';

    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
    protected $guarded = ['id'];

    /**
    * Attributes that are primary field.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['created_at', 'updated_at'];


    /**
    * The attributes that are appends.
    *
    * @var array
    */
    protected $appends = [
        'status_html',
        'sort_message',
        'simple_date',
        'full_date',
    ];

    /**
    * Get Unit Kerja from db units.
    *
    * @return string
    */
    public function getStatusHtmlAttribute()
    {
        if ($this->status != 1) {
            return '<div class="badge badge-pill badge-light-secondary mr-1">belum dibaca</div>';
        }

        return '<div class="badge badge-pill badge-light-success mr-1">telah dibaca</div>';
    }

    /**
    *
    * @return string
    */
    public function getSortMessageAttribute()
    {
        $string = strip_tags($this->message);
        $maxString = 120;

        if (strlen($string) > $maxString)
        {
            // truncate string
            $stringCut = substr($string, 0, $maxString);
            $endPoint = strrpos($stringCut, ' ');

            //if the string doesn't contain any space then it will cut without word basis.
            $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
            $string .= '...';
        }

        return $string;
    }

    /**
    *
    * @return string
    */
    public function getSimpleDateAttribute()
    {
        return date_format($this->created_at, "d M Y");
    }

    /**
    *
    * @return string
    */
    public function getFullDateAttribute()
    {
        return date_format($this->created_at, "H:i | d M Y");
    }
}
