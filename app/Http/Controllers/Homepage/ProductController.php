<?php

namespace App\Http\Controllers\Homepage;

use App\Models\Post;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();

        $collectCategory = collect($categories);
        $groupCategory = $collectCategory->map(function ($item) {
            $item->products = $this->fetchByCategory($item->id);
            return $item;
        });

        $datas = [
            'category' => Category::all(),
            'groupCategory' => $groupCategory,
        ];

        return view('frontend.pages.categories', $datas);
    }

    /**
     * Fetch product by Category.
     *
     * @param String categoryId
     * @return Array
     */
    public function fetchByCategory($categoryId)
    {
        return Post::where('category', $categoryId)->get();
    }

    public function search(Request $request)
    {
        $name = $request->input('name');
        if (empty($name)) return redirect('category');

        $datas = [
            'category' => Category::all(),
            'products' => Post::where('title', 'like', '%'. $name .'%')->paginate(100),
        ];

        return view('frontend.pages.product-category', $datas);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function productCategory($category)
    {
        $category = Category::where('slug', $category)->first();
        $datas = [
            'products' => Post::where('category', $category->id)->paginate(100),
            'category' => Category::all(),
        ];

        return view('frontend.pages.product-category', $datas);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function productDetail($category, $product)
    {
        $product =  Post::where('slug', $product)->first();

        $datas = [
            'product' => $product,
            'category' => Category::all(),
            'og_title' => $product->title,
            'og_description' => $product->first_paragraph,
            'og_url' => $product->url,
            'og_image' => URL(env('PATH_POST') .'/'. $product->image),
        ];

        return view('frontend.pages.product-detail', $datas);
    }
}
