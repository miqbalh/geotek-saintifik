<?php

namespace App\Http\Controllers\Homepage;

use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = Customer::all();

        $datas = [
            'collection' => $customer,
        ];

        return view('homepage.pages.customer.customer', $datas);
    }
}
