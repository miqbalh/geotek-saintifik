<?php

namespace App\Http\Controllers\Homepage;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\AskQuestions;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = [
            'category' => Category::all(),
        ];

        return view('frontend.pages.contact-us', $datas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $error_message  = false;

        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'name' => 'bail|required',
                'email' => 'bail|required|email',
                'company_name' => 'bail|required',
                'phone' => 'bail|required',
                'message' => 'bail|required',
            ]);

            if ($validator->fails()) {
                $error_message = $validator->errors()->all()[0];
            } else {
                $question = new AskQuestions;
                $question->name = $request->get('name');
                $question->email = $request->get('email');
                $question->company_name = $request->get('company_name');
                $question->phone = $request->get('phone');
                $question->message = $request->get('message');
                $question->save();

                return redirect('/contact-us')->with('success_message', 'Congratulations. Your message has been sent successfully');
            }
        }

        $data = [
            'category' => Category::all(),
            'error_message' => $error_message,
            'input'         => $request->input(),
        ];

        return view('frontend.pages.contact-us', $data);
    }
}
