<?php

namespace App\Http\Controllers\Homepage;

use App\Models\Post;
use App\Models\Category;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (env('IS_MAINTENANCE')) {
            return view('homepage.pages.static.maintenance');
        } else {
            $datas = [
                'banner' => Post::where('is_homepage', true)->get(),
                'products' => Post::orderBy('created_at', 'DESC')->paginate(12),
                'category' => Category::all(),
                'customer' => Customer::all(),
            ];

            return view('frontend.pages.home-v2', $datas);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function about()
    {
        $datas = [
            'category' => Category::all(),
        ];

        return view('frontend.pages.about', $datas);
    }
}
