<?php

namespace App\Http\Controllers\Dashboard;

use Validator;
use App\Models\Post;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::all();
        $category = Category::all();

        $datas = [
            'collection' => $post,
            'category' => $category,
        ];

        return view('dashboard.pages.post.index', $datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $error_message  = false;
        $category       = Category::all();

        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'title'         => 'bail|required',
                'category'      => 'bail|required',
                'description'   => 'bail|required',
                'image'         => 'bail|required|mimes:jpeg,jpg,png|max:1000',
                'is_homepage'   => 'bail|required',
            ]);

            if ($validator->fails()) {
                $error_message = $validator->errors()->all()[0];
            } else {
                if ($request->hasFile('image')){
                    $img_path   = public_path(env('PATH_POST'));
                    $name       = date_time();

                    \File::exists($img_path) or \File::makeDirectory($img_path);

                    $img_name = $name.'.'.$request->file('image')->getClientOriginalExtension();
                    $request->file('image')->move($img_path, $img_name);
                }

                if ($request->hasFile('cover')){
                    $img_path_cover = public_path(env('PATH_POST_COVER'));
                    $name_cover       = Str::slug($request->get('title'));

                    \File::exists($img_path_cover) or \File::makeDirectory($img_path_cover);

                    $img_name_cover = $name_cover.'.'.$request->file('cover')->getClientOriginalExtension();
                    $request->file('cover')->move($img_path_cover, $img_name_cover);
                }

                $post = new Post;
                $post->title        = $request->get('title');
                $post->slug         = Str::slug($request->get('title'));
                $post->category     = $request->get('category');
                $post->description  = $request->get('description');
                $post->image        = $img_name;
                $post->cover        = $img_name_cover;
                $post->is_homepage  = $request->get('is_homepage');
                $post->save();

                return redirect('/admin/post')->with('success_message', 'Post successfully added.');
            }
        }

        $data = [
            'category'      => $category,
            'error_message' => $error_message,
            'input'         => $request->input()
        ];

        return view('dashboard.pages.post.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $error_message  = false;
        $post           = Post::find($id);
        $category       = Category::all();

        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'title'         => 'bail|required',
                'category'      => 'bail|required',
                'description'   => 'bail|required',
                'image'         => 'bail|mimes:jpeg,jpg,png|max:1000',
                'is_homepage'   => 'bail|required',
            ]);

            if ($validator->fails()) {
                $error_message = $validator->errors()->all()[0];
            } else {
                if ($request->hasFile('image')){
                    $img_path   = public_path(env('PATH_POST'));
                    $name       = date_time();
                    $img_old    = $post->image;

                    \File::exists($img_path) or \File::makeDirectory($img_path);

                    if(!empty($img_old)){
                        if(\File::isFile($img_path.$img_old)){
                            \File::delete($img_path.$img_old);
                        }
                    }

                    $img_name = $name.'.'.$request->file('image')->getClientOriginalExtension();
                    $request->file('image')->move($img_path, $img_name);

                    $post->image    = $img_name;
                }

                if ($request->hasFile('cover')){
                    $img_path_cover = public_path(env('PATH_POST_COVER'));
                    $name_cover = Str::slug($request->get('title'));
                    $img_old_cover = $post->cover;

                    \File::exists($img_path_cover) or \File::makeDirectory($img_path_cover);

                    if(!empty($img_old_cover)){
                        if(\File::isFile($img_path_cover.$img_old_cover)){
                            \File::delete($img_path_cover.$img_old_cover);
                        }
                    }

                    $img_name_cover = $name_cover.'.'.$request->file('cover')->getClientOriginalExtension();
                    $request->file('cover')->move($img_path_cover, $img_name_cover);

                    $post->cover    = $img_name_cover;
                }

                $post->title        = $request->get('title');
                $post->slug         = Str::slug($request->get('title'));
                $post->category     = $request->get('category');
                $post->description  = $request->get('description');
                $post->is_homepage  = $request->get('is_homepage');
                $post->save();

                return redirect('/admin/post')->with('success_message', 'Post successfully updated.');
            }
        }

        $data = [
            'form'          => $post,
            'category'      => $category,
            'error_message' => $error_message,
            'input'         => $request->input()
        ];

        return view('dashboard.pages.post.update', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post       = Post::find($id);
        $img_path   = public_path(env('PATH_POST'));
        $img_old    = $post->image;

        if(!empty($img_old)){
            if(\File::isFile($img_path.$img_old)){
                \File::delete($img_path.$img_old);
            }
        }

        $img_path_cover = public_path(env('PATH_POST_COVER'));
        $img_old_cover = $post->cover;

        if(!empty($img_old_cover)){
            if(\File::isFile($img_path_cover.$img_old_cover)){
                \File::delete($img_path_cover.$img_old_cover);
            }
        }

        $post->delete();

        return \Response::json();
    }
}
