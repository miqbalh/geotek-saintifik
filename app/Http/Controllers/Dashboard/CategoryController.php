<?php

namespace App\Http\Controllers\Dashboard;

use Validator;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::all();

        $datas = [
            'collection' => $category,
        ];

        return view('dashboard.pages.category.index', $datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $error_message  = false;

        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'name'      => 'bail|required',
                'parent'    => 'bail',
                'image'     => 'bail||mimes:jpeg,jpg,png|max:1000',
            ]);

            if ($validator->fails()) {
                $error_message = $validator->errors()->all()[0];
            } else {
                if ($request->hasFile('image')){
                    $img_path   = public_path(env('PATH_CATEGORY'));
                    $name       = date_time();

                    \File::exists($img_path) or \File::makeDirectory($img_path);

                    $img_name = $name.'.'.$request->file('image')->getClientOriginalExtension();
                    $request->file('image')->move($img_path, $img_name);
                }

                $category = new Category;
                $category->name     = $request->get('name');
                $category->slug     = Str::slug($request->get('name'));
                $category->parent   = empty($request->get('parent')) ? 0 : $request->get('parent');
                $category->image    = $img_name ?? '';
                $category->save();

                return redirect('/admin/category')->with('success_message', 'Category successfully added.');
            }
        }

        $category = Category::all();

        $data = [
            'category'      => $category,
            'error_message' => $error_message,
            'input'         => $request->input()
        ];

        return view('dashboard.pages.category.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $error_message  = false;
        $category   = Category::find($id);

        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'name'      => 'bail|required',
                'parent'    => 'bail',
                'image'     => 'mimes:jpeg,jpg,png|max:1000',
            ]);

            if ($validator->fails()) {
                $error_message = $validator->errors()->all()[0];
            } else {
                if ($request->hasFile('image')){
                    $img_path   = public_path(env('PATH_CATEGORY'));
                    $name       = date_time();
                    $img_old    = $category->image;

                    \File::exists($img_path) or \File::makeDirectory($img_path);

                    if(!empty($img_old)){
                        if(\File::isFile($img_path.$img_old)){
                            \File::delete($img_path.$img_old);
                        }
                    }

                    $img_name = $name.'.'.$request->file('image')->getClientOriginalExtension();
                    $request->file('image')->move($img_path, $img_name);

                    $category->image    = $img_name;
                }

                $category->name         = $request->get('name');
                $category->slug         = Str::slug($request->get('name'));
                $category->parent       = empty($request->get('parent')) ? 0 : $request->get('parent');
                $category->save();

                return redirect('/admin/category')->with('success_message', 'Category successfully updated.');
            }
        }

        $categories = Category::all();

        $data = [
            'form'          => $category,
            'category'      => $categories,
            'error_message' => $error_message,
            'input'         => $request->input()
        ];

        return view('dashboard.pages.category.update', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        $img_path   = public_path(env('PATH_CATEGORY'));
        $img_old    = $category->image;

        if(!empty($img_old)){
            if(\File::isFile($img_path.$img_old)){
                \File::delete($img_path.$img_old);
            }
        }

        $category->delete();

        return \Response::json();
    }
}
