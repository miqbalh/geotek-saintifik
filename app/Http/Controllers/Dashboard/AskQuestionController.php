<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AskQuestions;

class AskQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = AskQuestions::all();

        $datas = [
            'collection' => $list,
        ];

        return view('dashboard.pages.ask-question.index', $datas);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [
            'collection' => AskQuestions::find($id),
        ];

        return view('dashboard.pages.ask-question.show', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = AskQuestions::find($id);

        $data->delete();

        return \Response::json();
    }
}
