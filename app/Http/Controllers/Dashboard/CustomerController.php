<?php

namespace App\Http\Controllers\Dashboard;

use Validator;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = Customer::all();

        $datas = [
            'collection' => $customer,
        ];

        return view('dashboard.pages.customer.index', $datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $error_message  = false;

        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'name'      => 'bail|required',
                'image'     => 'bail|mimes:jpeg,jpg,png|max:1000',
            ]);

            if ($validator->fails()) {
                $error_message = $validator->errors()->all()[0];
            } else {
                if ($request->hasFile('logo')){
                    $img_path   = public_path(env('PATH_CUSTOMER'));
                    $name       = date_time();

                    \File::exists($img_path) or \File::makeDirectory($img_path);

                    $img_name = $name.'.'.$request->file('logo')->getClientOriginalExtension();
                    $request->file('logo')->move($img_path, $img_name);
                }

                $customer = new customer;
                $customer->name     = $request->get('name');
                $customer->logo    = $img_name ?? '';
                $customer->save();

                return redirect('/admin/customer')->with('success_message', 'Customer successfully added.');
            }
        }

        $data = [
            'error_message' => $error_message,
            'input'         => $request->input()
        ];

        return view('dashboard.pages.customer.create', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $error_message  = false;
        $customer   = Customer::find($id);

        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'name'      => 'bail|required',
                'logo'      => 'mimes:jpeg,jpg,png|max:1000',
            ]);

            if ($validator->fails()) {
                $error_message = $validator->errors()->all()[0];
            } else {
                if ($request->hasFile('logo')){
                    $img_path   = public_path(env('PATH_CUSTOMER'));
                    $name       = date_time();
                    $img_old    = $customer->logo;

                    \File::exists($img_path) or \File::makeDirectory($img_path);

                    if(!empty($img_old)){
                        if(\File::isFile($img_path.$img_old)){
                            \File::delete($img_path.$img_old);
                        }
                    }

                    $img_name = $name.'.'.$request->file('logo')->getClientOriginalExtension();
                    $request->file('logo')->move($img_path, $img_name);

                    $customer->logo    = $img_name;
                }

                $customer->name         = $request->get('name');
                $customer->save();

                return redirect('/admin/customer')->with('success_message', 'customer successfully updated.');
            }
        }

        $data = [
            'form'          => $customer,
            'error_message' => $error_message,
            'input'         => $request->input()
        ];

        return view('dashboard.pages.customer.update', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);

        $img_path   = public_path(env('PATH_CUSTOMER'));
        $img_old    = $customer->logo;

        if(!empty($img_old)){
            if(\File::isFile($img_path.$img_old)){
                \File::delete($img_path.$img_old);
            }
        }

        $customer->delete();

        return \Response::json();
    }
}
