<?php

use Carbon\Carbon;
use App\Models\Category;

/**
 * Date Now timestamp format
 *
 * @return date
 */
if (!function_exists('format_timestamp'))
{
  function format_timestamp()
  {
    $date = carbon::now();
    $date = $date->format('Y-m-d H:i:s');
    return $date;
  }
}

/**
 * Date Time Now
 *
 * @param  date
 * @return true false
 */
if (!function_exists('date_time'))
{
  function date_time()
  {
    $date = carbon::now();
    $date = Carbon::parse($date);
    $date = $date->format('d-m-Y_H-i-s');
    return $date;
  }
}

/**
 * Fetch category
 *
 * @return obj
 */
if (!function_exists('fetch_category'))
{
  function fetch_category()
  {
    $category = Category::all();

    return $category;
  }
}


/**
 * Set recursive loop for menu product
 *
 * params String / Array
 * return class name
 */
if (!function_exists('isMenuActive'))
{
  function isMenuActive($routesName)
  {
    if (getType($routesName) == 'string') {
      $condition = Route::currentRouteName() === $routesName;
    } else {
      $condition = in_array(Route::currentRouteName(), $routesName);
    }

    $isActive = $condition ? 'active text-dark' : '';
    return $isActive;
  }
}
